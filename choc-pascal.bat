REM git minimum
choco install git -y

REM lokale sprachpackete golang
choco install freepascal -y
choco install lazarus -y

REM Visual Studio Code mit einigen Plugins für Go
choco install vscode -y
rem choco install vscode-pascal -y # manual / online https://marketplace.visualstudio.com/items?itemName=alefragnani.pascal
rem choco install vscode-pascal-formatter -y # manual / online https://marketplace.visualstudio.com/items?itemName=alefragnani.pascal-formatter
rem choco install vscode-fpdebug -y # manual / online https://marketplace.visualstudio.com/items?itemName=CNOC.fpdebug
choco install vscode-markdownlint -y




