

REM ## Microsoft SQL Server 2022 Developer Edition 16.0.1000.6
choco install sql-server-2022 y


REM ## Microsoft SQL Server 2022 Express with Advanced Services 16.0.1000.6
REM ## Microsoft SQL Server 2022 Express 2022.16.0.1000
choco install sql-server-express-adv y
rem choco install sql-server-express -y

REM ## SQL Server Management Studio 17.9.1 14.0.17289.1
choco install sql-server-management-studio -y

REM ## Microsoft SQL Server Migration Assistant for Access (Install) 9.0.0
choco install microsoft-sql-server-migration-assistant-for-access -y


rem # Microsoft SQL Server Data Tools for Visual Studio 2017 14.0.16186.0
rem choco install ssdt17