REM ################################################################
REM FileTools
REM ################################################################

choco install 7zip -y

choco install virtualclonedrive -y

choco install qfinderpro -y


REM ################################################################
REM TC
REM ################################################################
choco install totalcommander -y
wget https://files.itk-trainer.de/index.php/s/ip99pqsb5sj848s/download/file.sb -O ./cfg/wincmd.key
rem XCOPY /Y .\cfg\wincmd.* "c:\totalcmd\"
rem XCOPY /Y .\cfg\wincmd.* "c:\Program Files\totalcmd\"
XCOPY /Y .\cfg\wincmd.* "c:\Users\%username%\AppData\Roaming\GHISLER\"


REM ################################################################
REM Editoren
REM ################################################################

REM notepad++
choco install notepadplusplus -y
rem choco install notepadplusplus.settings -force -params 'url=https://yoursite.com/NotepadSettings/'
rem https://community.chocolatey.org/packages/Notepadplusplus.Settings
XCOPY /Y .\cfg\config.xml "c:\Users\%username%\AppData\Roaming\Notepad++\"

REM notepad3 
choco install notepad3 -y
XCOPY /Y .\cfg\Notepad3.ini c:\Users\%username%\AppData\Roaming\Rizonesoft\Notepad3\Notepad3.ini

REM ################################################################
REM PDF Viewer
REM ################################################################

REM Evince badly moved the package to another destination
rem choco install evince -y 
wget https://github.com/tim-lebedkov/packages/releases/download/2020_07/evince-2.32.0.145.msi -P %userprofile%/Downloads
%userprofile%\Downloads\evince-2.32.0.145.msi /qn /i
XCOPY /Y .\cfg\evince_toolbar.xml "c:\Users\%username%\.gnome2\evince\"


REM Adobe PDF Reader
choco install adobereader -y