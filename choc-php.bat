REM git minimum
choco install git -y


choco install wget -y
choco install curl -y

REM lokale sprachpackete perl
choco install php -y
choco install composer -y

REM Visual Studio Code mit einigen Plugins
choco install vscode -y
rem choco install vscode-php -y  # manuell / online Installieren https://marketplace.visualstudio.com/items?itemName=bmewburn.vscode-intelephense-client
rem choco install vscode-php-debug -y  # manuell / online Installieren https://marketplace.visualstudio.com/items?itemName=xdebug.php-debug
rem choco install vscode-remote-development -y  # manuell / online Installieren https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.vscode-remote-extensionpack
choco install vscode-markdownlint -y
choco install vscode-docker -y

REM Telerik Fiddler Web Debugger 5.0.20211.51073
choco install fiddler -y

choco install db-visualizer -y
choco install heidisql -y


choco install postgresql -y
choco install pgadmin4 -y
choco install pgcli -y


choco install postgrest -y

REM An Http REST Client and API-Testing Tool for Windows
choco install postman -y
choco install postman-cli -y


REM Maybe you need ...
rem choco install docker-desktop -y # bad idea !

rem # Setup
rem Das Setup besteht aus den folgenden Schritten
rem 
rem 1. Starten der WSL distribution (z.B. Ubuntu)
rem 2. Installieren von Podman in WSL
rem 3. Installieren von OpenSSH in WSL und Generierung der Keys
rem 4. Services so einrichten, dass sie beim WSL Start automatisch Starten
rem 5. Installieren von Podman in Windows

choco install wsl -y
rem see https://www.devcon5.ch/blog/2021/10/14/podman-for-windows/


REM and/or

choco install xampp-81 -y

rem Working with https://laravel.com/docs/9.x/installation#getting-started-on-windows
rem or
rem Working with https://symfony.com/doc/current/setup.html#installing-packages
rem choco install symfony-cli



REM https://www.javatpoint.com/docker-php-example
