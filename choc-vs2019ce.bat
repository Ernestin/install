
rem choco install dotnetcoresdk

rem choco install netfx-4.7.2-devpack -y

choco install visualstudio2019buildtools -y
rem choco install visualstudio2019teamexplorer -y 

choco install visualstudio2019community -y
rem choco install visualstudio2019professional -y
rem choco install visualstudio2019enterprise -y


rem ### netcore ###
rem choco install visualstudio2019-workload-netcoretools -y
rem choco install visualstudio2019-workload-netcorebuildtools -y

rem ### web ###
choco install visualstudio2019-workload-netweb -y
rem choco install visualstudio2019-workload-azure -y
choco install visualstudio2019-workload-webbuildtools -y
rem choco install visualstudio2019-workload-nodebuildtools -y
rem choco install visualstudio2019-workload-netcrossplat -y

rem ### desktop ###
choco install visualstudio2019-workload-manageddesktop -y
choco install visualstudio2019-workload-universal -y

rem ### office ###
choco install visualstudio2019-workload-office -y

rem ### mobile c++ ###
rem choco install visualstudio2019-workload-nativemobile -y

rem ### C++ ###
rem choco install visualstudio2019-workload-nativecrossplat -y
rem choco install visualstudio2019-workload-vctools -y
choco install visualstudio2019-workload-nativedesktop -y
rem choco install visualstudio2019-workload-visualstudioextension -y

rem ### SQL DATA ###
rem choco install visualstudio2019-workload-data -y
rem choco install visualstudio2019-workload-databuildtools -y
rem choco install visualstudio2019-workload-datascience -y

rem choco install ssis-vs2019 -y

rem choco install visualstudio2019-workload-python -y

