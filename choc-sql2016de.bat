


rem ## Microsoft SQL Server 2016 Developer Edition 
choco install sql-server-2016-developer-edition -y

rem ## Microsoft SQL Server 2017 Reporting Services 14.0.600.1109
rem choco install ssrs

rem # Microsoft SQL Server Data Tools for Visual Studio 2015 14.0.16186.0
rem choco install ssdt15 -y
rem # Microsoft SQL Server Data Tools for Visual Studio 2017 14.0.16186.0
rem choco install ssdt17 -y

rem ## SQL Server 2017 Express LocalDB 14.0.1000.169
rem choco install sqllocaldb -y

rem ## SQL Server Management Studio 17.9.1 14.0.17289.1
choco install sql-server-management-studio -y

