
rem ### .netfx 4.7.2  ###
choco install netfx-4.7.2-devpack -y
choco install netfx-4.7.2 -y

choco install netfx-4.8.1-devpack -y
choco install netfx-4.8.1 -y


rem ### .net 7 runtime ###
choco install dotnet-7.0-runtime -y
choco install dotnet-7.0-desktopruntime -y
choco install dotnet-7.0-aspnetruntime -y
choco install dotnet-7.0-windowshosting -y

rem ### .net 7 sdk ###
choco install dotnet-7.0-sdk -y
choco install dotnet-7.0-sdk-1xx -y
choco install dotnet-7.0-sdk-2xx -y



REM ### Tools for Build-Server/Test-Server needs git as well
rem choco install visualstudio2022buildtools -y
rem choco install visualstudio2022teamexplorer -y 
rem choco install visualstudio2022testagent -y


rem ### Visual Studio ###
choco install visualstudio2022community -y
rem choco install visualstudio2022professional -y
rem choco install visualstudio2022enterprise -y

rem ### Visual Studio PlugIn ###


rem ### mobile ###
choco install visualstudio2022-workload-netcrossplat -y

rem ### web ###
choco install visualstudio2022-workload-netweb -y
rem choco install visualstudio2022-workload-azure -y
choco install visualstudio2022-workload-webbuildtools -y
choco install visualstudio2022-workload-nodebuildtools -y
choco install visualstudio2022-workload-node -y

rem ### desktop ###
choco install visualstudio2022-workload-manageddesktop -y
choco install visualstudio2022-workload-universal -y

rem ### office ###
choco install visualstudio2022-workload-office -y

rem ### mobile c++ ###
rem choco install visualstudio2022-workload-nativemobile -y

rem ### C++ ###
choco install visualstudio2022-workload-nativecrossplat -y
choco install visualstudio2022-workload-vctools -y
choco install visualstudio2022-workload-nativedesktop -y
choco install visualstudio2022-workload-visualstudioextension -y

rem ### SQL DATA ###
choco install visualstudio2022-workload-data -y
choco install visualstudio2022-workload-databuildtools -y
rem choco install visualstudio2022-workload-datascience -y

rem ### Python ###
choco install visualstudio2022-workload-python -y

REM ### VS EXTEND ###
REM Tools for building add-ons and extensions for Visual Studio, including new commands, code analyzers and tool windows.
choco install visualstudio2022-workload-visualstudioextensionbuildtools -y


REM VS-CMD Tools.ImportandExportSettings /export:"c:\Files\MyFile.vssettings"


choco install NugetPackageExplorer -y
choco install msbuild-structured-log-viewer -y


rem choco install wsl -y
rem choco install wsl-ubuntu-2204 -y

