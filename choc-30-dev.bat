
choco install winmerge -y


REM GIT Tools
choco install git -y
choco install git-lfs -y
choco install git-credential-manager-for-windows -y
choco install gitdiffmargin -y


REM Visual Studio Code
rem choco install vscode -y
rem choco install vscode-gitlens -y


REM ATOM Editor
rem A hackable text editor for the 21st Century.
rem choco install atom -y

REM ILSpy is the open-source .NET assembly browser and decompiler.
REM https://github.com/icsharpcode/ILSpy
choco install ilspy -y

REM Vut is a utility for propagating version numbers.
REM https://github.com/forbjok/vut
choco install vut -y


REM An open-source x64/x32 debugger for windows.
rem choco install x64dbg.portable -y

REM Debugging Tools for Windows (windbg) 10.0.10586.15
REM Microsoft Windows Debugger (WinDbg) is a powerful Windows-based debugger that is capable of both user-mode and kernel-mode debugging. 
rem choco install windbg -Y


REM https://github.com/RedisInsight/RedisDesktopManager
REM   https://redis.com/blog/so-youre-looking-for-the-redis-gui/
REM https://github.com/lework/RedisDesktopManager-Windows/releases
rem choco install redis-desktop-manager -y
rem choco install win-rdm -y

REM https://github.com/qishibo/AnotherRedisDesktopManager
