REM git minimum
choco install git -y

REM lokale sprachpackete golang
choco install golang -y
choco install go-task -y

REM Visual Studio Code mit einigen Plugins für Go
choco install vscode -y
choco install vscode-go -y
choco install vscode-markdownlint -y
choco install vscode-docker -y



