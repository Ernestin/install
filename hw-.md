Mobiler IT-Seminarraum
======================

Für Schulungen außerhalb eines Schulungscenters bieten wir unsere mobile Ausstattung an.
Neben einem Beamer, können Sie nach Bedarf eine bestimmte anzahl an Notebook Computern mit der passenden Installationen dazu mieten.
Der Preise variieren hier je nach Anzahl der Teilnehmer, der Tage, sowie dem Installationsaufwand.
Bei von uns durchgeführten Kursen wird kein Installationsaufwand veranschlagt.

Preise verstehen sich inkl. Versandkosten Deutschland (ohne Inselzuschlag) und zzgl MwSt.


Preise (Technische Details siehe unten):
----------------------------------------
[ ] Beamer                 40,-€ pro Tag    oder  150,-€ die Woche
[ ] NAS/Server             40,-€ pro Tag    oder  150,-€ die Woche

[ ] Notebook & Maus*       25,-€ pro Tag    oder  100,-€ die Woche    
[ ] Notebook & Maus        40,-€ pro Tag    bzw.  200,-€ die Woche
 
[ ] zusätzlicher Monitor   12,-€ pro Tag
[ ] zusätzliche Tastatur    8,-€ pro Tag     

[ ] Netzwerk Switch GBit LAN      10,-€ pro Tag     


[ ] Win10-Basis** Installation        0,-€ einmalig  
[ ] Linux-Basis** Installation        0,-€ einmalig
[ ] FreeBSD-Basis** Installation     10,-€ einmalig
[ ] SolarisX86**  Installation       40,-€ einmalig
[ ] Win10-Libre** Installation       20,-€ einmalig  
[ ] Linux-Libre** Installation        0,-€ einmalig
[ ] Win10-Office** Installation      35,-€ einmalig  
[ ] Win10-VS2017** Installation      20,-€ einmalig  
[ ] Linux-Coder** Installation       20,-€ einmalig
[ ] Win10-Java** Installation        20,-€ einmalig  
[ ] Linux-Java** Installation        20,-€ einmalig
[ ] Benutzerdefinierte Installation  nach Aufwand


Miet dauer vom --.--.---- bis --.--.---- bzw entsprechend Tage: __


*  Wenn ein abschließbarer Raum mit ausgehändigtem Schlüssel vorhanden ist
** Siehe Software Installations Details unten


Technische Details:
-------------------
Beamer
- Anschlüsse: VGA + DVI
- Auflösung:  1440x1024

Schulungsserver / NAS
- Marke: QNAP TS-251+ mit 16GB RAM
- 2x 750GB HDD
- Diverse DockerVMs per Click lauffähig zB GitLab
- Eigene Windows VMs (Virtualization Station)
  - zB. Windows-Server mit SQL und Reporting Service
  - zB. SharePoint
  - zB. Microsoft Navision EPR
  etc. p.p.
- oder Linux VMs (LXC)
  - Apache Tomcat
  - Oracle Glassfish
  - Nginx WebServer
  etc. p.p.
 

Notebooks
- Marke: IBM/Lenovo Thinkpad T530
- CPU: 2x2,6GHz
- RAM: 16GB
- SSD: 128GB
- WLAN: Ja
- USB3: 2x
- Mouse: Drei-Tasten-Rad Opt.

Monitore
- Marke: Samsung 24xx
- Größe: ~24" (23,8")
- Auflösung: bis FHD (1920x1080)
- samt Stromkabel bzw. Netzteil und Anschlußkabel für Notebook

Tastatur
- Marke: Cherry GS80E
- Anschluss: USB (opt. PS2)

Netzwerk Switch managebar
- Marke: Netgear GS108Tv2
- samt Stromkabel und Netzteil
- samt gestaffelter Kabel der längen 1m/2m/3m für je ein Noteboook


Software Installations Details:
-------------------------------

Win10-Basis
- Aktuelles Update (1805)
- Alle  Updates, Patches, Fixes und Treiber installiert
- VirtualBox 5.2.x
- Notepad++
- Total Commander

Linux-Basis
- Distro: Ubuntu 18.04LTS oder CentOS 7.5 (1804) oder Ubuntu MATE 18.04
- VirtualBox
- LibreOffice
- gEdit

...