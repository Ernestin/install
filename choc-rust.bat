REM git minimum
choco install git -y

choco install curl -y

REM lokale sprachpackete golang
rem choco install rustup.install -y
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh


REM Visual Studio Code mit einigen Plugins für Go
choco install vscode -y
choco install vscode-rust-test-adapter -y
rem choco install vscode-rust-lang -y # manual / online https://marketplace.visualstudio.com/items?itemName=rust-lang.rust-analyzer
rem choco install vscode-rust-crates -y # manual / online https://marketplace.visualstudio.com/items?itemName=serayuzgur.crates
rem choco install vscode-better-toml -y # manual / online https://marketplace.visualstudio.com/items?itemName=bungcip.better-toml
choco install vscode-markdownlint -y
choco install vscode-docker -y

rem cargo
rem crates  https://crates.io/

