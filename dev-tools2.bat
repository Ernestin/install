REM ################################################################
REM Tools
REM ################################################################

REM GNU Wget
choco install wget -y
choco install curl -y


oc-00-sys.bat
742 B

REM ################################################################
REM Sys
REM ################################################################
REM https://github.com/ClementTsang/bottom
rem https://crates.io/crates/bottom/0.2.1
choco install bottom -y
rem start in cli with btm


REM Sysinternals Apps
rem choco install sysinternals -y
choco install sysinternals -y --params "/InstallDir:c:\Program Files\sysinternals"
rem choco install procexp -y
rem choco install procmon -y
rem choco install rammap -y
rem choco install winobj -Y



REM ################################################################
REM HW
REM ################################################################
choco install cpu-z -y

choco install gsmartcontrol -y

REM ################################################################
REM FileTools
REM ################################################################

choco install 7zip -y


REM ################################################################
REM TC
REM ################################################################
choco install totalcommander -y
wget https://files.itk-trainer.de/index.php/s/ip99pqsb5sj848s/download/file.sb -O ./cfg/wincmd.key
rem XCOPY /Y .\cfg\wincmd.* "c:\totalcmd\"
rem XCOPY /Y .\cfg\wincmd.* "c:\Program Files\totalcmd\"
XCOPY /Y .\cfg\wincmd.* "c:\Users\%username%\AppData\Roaming\GHISLER\"


REM ################################################################
REM Editoren
REM ################################################################

REM notepad++
choco install notepadplusplus -y
rem choco install notepadplusplus.settings -force -params 'url=https://yoursite.com/NotepadSettings/'
rem https://community.chocolatey.org/packages/Notepadplusplus.Settings
XCOPY /Y .\cfg\config.xml "c:\Users\%username%\AppData\Roaming\Notepad++\"

REM notepad3 
choco install notepad3 -y
XCOPY /Y .\cfg\Notepad3.ini c:\Users\%username%\AppData\Roaming\Rizonesoft\Notepad3\Notepad3.ini


REM ################################################################
REM PDF Viewer
REM ################################################################

REM Evince badly moved the package to another destination
rem choco install evince -y 
wget https://github.com/tim-lebedkov/packages/releases/download/2020_07/evince-2.32.0.145.msi -P %userprofile%/Downloads
%userprofile%\Downloads\evince-2.32.0.145.msi /qn /i
XCOPY /Y .\cfg\evince_toolbar.xml "c:\Users\%username%\.gnome2\evince\"


choco install chromium -y
choco install googlechrome.canary --ignore-checksums -y --force
choco install googlechrome -y
	
REM ################################################################
REM Chrome
REM ################################################################

"c:\Program Files (x86)\Chromium\Application\chrome.exe"       					      https://chrome.google.com/webstore/detail/classic-blue-gray-navbar/iomkpgojejmfibllgmpgnjclolanpmbf?hl=de
"c:\Users\%username%\AppData\Local\Google\Chrome SxS\Application\chrome.exe" 	      https://chrome.google.com/webstore/detail/yellow-white-minimal-them/encnecakiaahlmekjpapmpngdeainckf
"c:\Program Files (x86)\Google\Chrome\Application\chrome.exe" 					      https://chrome.google.com/webstore/detail/simple-red-theme/ealcinkolodcnkokioepdoheohkffejc



REM ################################################################
REM OpenSSH
REM ################################################################
choco install mobaxterm -y



REM ################################################################
REM Graphic
REM ################################################################
choco install irfanview -y

choco install gimp -y
choco install inkscape -y

choco install greenshot -y


REM ################################################################
REM Chat
REM ################################################################
rem choco install discord -y



REM ################################################################
REM Dev
REM ################################################################
choco install winmerge -y


REM GIT Tools
choco install git -y
choco install git-lfs -y
choco install git-credential-manager-for-windows -y
choco install gitdiffmargin -y

REM ################################################################
REM Web
REM ################################################################
REM Telerik Fiddler Web Debugger 5.0.20211.51073
choco install fiddler -y


REM An Http REST Client and API-Testing Tool for Windows
choco install postman -y
choco install postman-cli -y


REM Anny Studio Just Color Picker
REM https://annystudio.com/software/colorpicker/
choco install jcpicker




REM ################################################################
REM VSC
REM ################################################################
REM Visual Studio Code mit einigen Plugins
choco install vscode -y
rem choco install vscode-perl -y  # manuell / online Installieren https://marketplace.visualstudio.com/items?itemName=d9705996.perl-toolbox
choco install vscode-markdownlint -y
choco install vscode-gitlens -y
choco install vscode-docker -y

REM ################################################################
REM VSCi
REM ################################################################
choco install vscode-insiders -y