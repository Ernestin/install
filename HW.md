[home](README.md) | [Minimale Raumausstattung](HW-MINI.md )
  
# Mobiler IT-Seminarraum

*Für Schulungen außerhalb eines Schulungscenters bieten wir unsere mobile Ausstattung an.
Neben einem Beamer, können Sie nach Bedarf eine bestimmte anzahl an Notebook Computern mit der passenden Installationen dazu mieten.
Der Preise variieren hier je nach Anzahl der Teilnehmer, der Tage, sowie dem Installationsaufwand.
Bei von uns durchgeführten Kursen wird kein Installationsaufwand veranschlagt.*

## Lokale Notebook Ausstattung


Sollten Ihre Geräte die Anforderungen nicht erfüllen, so können Sie bei uns auch Notebooks mieten (z.B):
- Lenovo Thinkpad T430s  -> https://thinkwiki.de/T430s
  - CPU: Core i5-3380M (2.60 GHz) 
  - RAM: 2x 8GB
  - DIS: 14" HD+ 1600x900 (LED Backlight)
  - SSD: 128GB
  - LAN: 1Gbit/S
  - WLAN: b/g/n
- Lenovo Thinkpad T430  -> https://thinkwiki.de/T430
  - CPU: Core i5-3380M (2.90 GHz) 
  - RAM: 1x 8GB
  - DIS: 14" HD+ 1600x900 (LED Backlight)
  - SSD: 250GB
  - LAN: 1Gbit/S
  - WLAN: b/g/n



## Lokales Netzwerk

Für einige Kurse sinnvoll, kann Ihnen eine LAN Infrastruktur im Einzelnen gestellt werden. So ist verfügbar:
- Router/Firewalls
  - z.B. eine einfache FritzBox 
  - ipFire Router/Firewall
  - pfSense Router/Firewall
  - DD-WRT Router/Firewall
- Switche 
  - Netgear GS108Tv2 Managed Switch mit VLAN und LAG
    - samt Netzteil/Stromkabel
    - samt gestaffelter Kabel der längen 1m/2m/3m für je ein Noteboook
- NAS
  - QNAP TS-251+ mit 16GB RAM und 2x 1TB SSD
    - Diverse DockerVMs per Click lauffähig zB GitLab
    - Eigene Windows VMs (Virtualization Station)
      - zB. Windows-Server mit SQL und Reporting Service
      - zB. SharePoint
      - zB. Microsoft Navision EPR
    - oder Linux VMs (LXC)
      - Apache Tomcat
      - Oracle Glassfish
      - Nginx WebServer



