REM git minimum
choco install git -y

REM lokale sprachpackete python
choco install python -y
rem choco install pip -y

REM Visual Studio Code mit einigen Plugins für Py
choco install vscode -y
choco install vscode-python -y
rem choco install vscode-python-test-adapter -y
rem choco install vscode-pylance -y
choco install vscode-markdownlint -y
choco install vscode-docker -y
choco install vscode-ansible -y
choco install vscode-puppet -y



