REM ################################################################
REM CLI Tools
REM ################################################################

REM GNU Wget
choco install wget -y
choco install curl -y



REM ################################################################
REM Windows Tools
REM ################################################################


REM Sysinternals Apps
rem choco install sysinternals -y
choco install sysinternals -y --params "/InstallDir:c:\Program Files\sysinternals"
rem choco install procexp -y
rem choco install procmon -y
rem choco install rammap -y
rem choco install winobj -Y



REM ################################################################
REM Utils
REM ################################################################

choco install irfanview -y

choco install greenshot -y


REM ################################################################
REM Terminals
REM ################################################################

REM OpenSSL
choco install openssl -y

REM SSH
choco install putty -y
choco install mobaxterm -y


REM ################################################################
REM Editoren
REM ################################################################

REM notepad++
choco install notepadplusplus -y
rem choco install notepadplusplus.settings -force -params 'url=https://yoursite.com/NotepadSettings/'
rem https://community.chocolatey.org/packages/Notepadplusplus.Settings
XCOPY /Y .\cfg\config.xml "c:\Users\%username%\AppData\Roaming\Notepad++\"

REM notepad3 
choco install notepad3 -y
XCOPY /Y .\cfg\Notepad3.ini c:\Users\%username%\AppData\Roaming\Rizonesoft\Notepad3\Notepad3.ini

REM ################################################################

choco install winmerge -y



REM ################################################################
REM Dev Tools
REM ################################################################

REM GIT Tools
choco install git -y
choco install git-lfs -y
choco install git-credential-manager-for-windows -y
choco install gitdiffmargin -y


REM Visual Studio Code
choco install vscode -y
choco install vscode-gitlens -y


REM ################################################################
REM API Tools
REM ################################################################

REM An Http REST Client and API-Testing Tool for Windows
choco install postman -y
choco install postman-cli -y

REM Telerik Fiddler Web Debugger 5.0.20211.51073
choco install fiddler -y

