

REM https://github.com/MaxBelkov/visualsyslog/
choco install visualsyslog -y

REM https://github.com/zarunbal/LogExpert
choco install logexpert -y

REM Tail
choco install snaketail.install -y
choco install baretail -y

REM logstash
choco install logstash -y


REM https://github.com/awaescher/Fusion
rem choco install fusionplusplus -y



REM https://podman-desktop.io/
choco install podman-desktop -y
