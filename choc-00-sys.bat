REM ################################################################
REM Tools
REM ################################################################

REM https://github.com/ClementTsang/bottom
rem https://crates.io/crates/bottom/0.2.1
choco install bottom -y
rem start in cli with btm


REM ################################################################
REM Utils
REM ################################################################

REM NETworkManager
choco install networkmanager -y

REM Sysinternals Apps
rem choco install sysinternals -y
choco install sysinternals -y --params "/InstallDir:c:\Program Files\sysinternals"
rem choco install procexp -y
rem choco install procmon -y
rem choco install rammap -y
rem choco install winobj -Y


