REM git minimum
choco install git -y

REM lokale sprachpackete perl
choco install strawberryperl -y
rem choco install activeperl -y #  well known for windows but propritary

REM Visual Studio Code mit einigen Plugins
choco install vscode -y
rem choco install vscode-perl -y  # manuell / online Installieren https://marketplace.visualstudio.com/items?itemName=d9705996.perl-toolbox
choco install vscode-markdownlint -y
choco install vscode-docker -y

REM Telerik Fiddler Web Debugger 5.0.20211.51073
choco install fiddler -y

rem choco install db-visualizer -y
choco install heidisql -y


choco install postgresql -y
choco install pgadmin4 -y
choco install pgcli -y

choco install postgrest -y


REM An Http REST Client and API-Testing Tool for Windows
choco install postman -y
choco install postman-cli -y


REM Maybe you need ...
rem choco install docker-desktop -y # bad idea !

rem # Setup
rem Das Setup besteht aus den folgenden Schritten
rem 
rem 1. Starten der WSL distribution (z.B. Ubuntu)
rem 2. Installieren von Podman in WSL
rem 3. Installieren von OpenSSH in WSL und Generierung der Keys
rem 4. Services so einrichten, dass sie beim WSL Start automatisch Starten
rem 5. Installieren von Podman in Windows

rem choco install wsl -y
rem see https://www.devcon5.ch/blog/2021/10/14/podman-for-windows/


REM and/or

rem choco install xampp-81 -y


REM https://www.javatpoint.com/docker-perl-example
