REM Modern, user-friendly command-line HTTP client for the API era
choco install httpie

REM An Http REST Client and API-Testing Tool for Windows
choco install postman -y
choco install postman-cli -y

REM https://docs.insomnia.rest/
choco install insomnia-rest-api-client -y

REM https://www.consul.io/
choco install consul -y

REM etcd is a distributed, consistent key-value store for shared configuration and service discovery
choco install etcd -y

