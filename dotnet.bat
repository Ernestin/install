REM ################################################################
REM dotnet framework
REM ################################################################

rem ### .netfx 4.8.1  ###
choco install netfx-4.8.1-devpack -y
choco install netfx-4.8.1 -y

rem ### .netfx 4.7.2  ###
rem choco install netfx-4.7.2-devpack -y
rem choco install netfx-4.7.2 -y



REM ################################################################
REM dotnet core
REM ################################################################


rem ### .net 8 runtime ###
rem choco install dotnet -y
choco install dotnet-8.0-runtime -y
choco install dotnet-8.0-desktopruntime -y
choco install dotnet-8.0-aspnetruntime -y
choco install dotnet-8.0-windowshosting -y

rem ### .net 8 sdk ###
rem choco install dotnet-sdk -y
choco install dotnet-8.0-sdk -y
choco install dotnet-8.0-sdk-1xx -y
choco install dotnet-8.0-sdk-2xx -y

rem ### .net 7 runtime ###
rem choco install dotnet-7.0-runtime -y
rem choco install dotnet-7.0-desktopruntime -y
rem choco install dotnet-7.0-aspnetruntime -y
rem choco install dotnet-7.0-windowshosting -y

rem ### .net 7 sdk ###
rem choco install dotnet-7.0-sdk -y
rem choco install dotnet-7.0-sdk-1xx -y
rem choco install dotnet-7.0-sdk-2xx -y
rem choco install dotnet-7.0-sdk-3xx -y
rem choco install dotnet-7.0-sdk-4xx -y
