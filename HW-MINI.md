[home](README.md) 

# Minimale Raumausstattung (primär Hardware)

| ...   | Description                 | Alternativ    |
| ---:  |    :----                    | :---          |
| CPU   | > 2x 1,6 GHz mit ivt[^note] |               |
| RAM   | >      8 GB                 |               |
| SSD   |    80 GB Frei               | auch HDD      |
| LAN   |     1 GBit/s Ethernet       | auch WLAN     |

- Maus/Tastatur und Bildschirm je Gerät.
- Ob LAN oder WLAN ein Internet-Zugang der Geräte ist notwendig!
- Für den Dozentennotebook
  - Beamer-Anschluss per HDMI oder VGA (andere Anschlüsse sind möglich aber bitte bescheid sagen damit der Richtige Adapter mitgenommen wird)
  - wenn Möglich separater FHD Monitor 20"-24" per HDMI oder VGA


[^note]: ivt = intel virtualisierung technology manchmal auch vt-x bitte mit dem 
         Microsoft Hardware-Assisted Virtualization Detection Tool 
         (https://www.microsoft.com/en-us/download/details.aspx?id=592) überprüfen,
         da auch bei geeigneter CPU das BIOS vt-x verhindern kann.
  
  
Siehe auch [Hardware](HW.md)