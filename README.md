# Install Anleitung [![License](https://img.shields.io/:license-apache-blue.svg)](https://opensource.org/licenses/Apache-2.0) 

### stand 2020-08 ohne binaries bis auf gnome/envince


## Inhalt
- [Install Anleitung ![License](https://opensource.org/licenses/Apache-2.0)](#install-anleitung-img-srchttpsimgshieldsiolicense-apache-bluesvg-altlicense)
    - [stand 2020-08 ohne binaries bis auf gnome/envince](#stand-2020-08-ohne-binaries-bis-auf-gnomeenvince)
  - [Inhalt](#inhalt)
  - [Repository Laden/Downloaden](#repository-ladendownloaden)
  - [Repository Entpacken](#repository-entpacken)
  - [choco Installieren](#choco-installieren)
  - [...](#)
  - [VirtualBox Host Installieren](#virtualbox-host-installieren)


*Vorgehensweise Allgemein:*
1. prüfen der [minimalen Hardwareanforderungen](HW-MINI.md) 
2. Downloaden
3. Entpacken
4. install-choco.bat
5. install-vbox-host.cmd

## Repository Laden/Downloaden
Entweder Sie laden das Repository über einen Browser **per URL https://gitlab.com/Ernestin/install und dann per Download**:
![](download.png)

Oder sie Clonen sich dieses Repoistory mit dem Befehl:

```bash
git clone https://gitlab.com/Ernestin/install.git
```

## Repository Entpacken
ZIP-Datei in geünschten Ordner kopieren. Dann im Explorer auf Datei mit rechter Maustaste "Alles extrahieren" wählen.
7zip und Co Funktionieren natürlich auch ...

![](shell-zip-Integration.png)



## choco Installieren

Die Scripte benutzen das Tool/den Dienst chocolatey (https://chocolatey.org) unter 
https://chocolatey.org/install
ist die installation erklärt oder viel einfacher **sie rufen über eine CMD-Shell welche Sie allerdings
"Als Administrator ausführen" öffnen müssen das Script install-choco.bat** auf wie in der folgenden Abbildung:

![](install-choco.png)


## ...
Nun können Sie nach belieben Pakete aus den Originalquellen (Wichtig denn Chocolately speichert nur die Installationsbeschreibung keine Software), wie Sie unter https://chocolatey.org/packages?q= suchen können installieren.
Je nach gewähltem installations Pfad, sollten Sie lokale Administrative Rechte haben und das CMD.EXE Fenster als Administrator öffnen.
Das Anhängen von ' -y' an die Befehlszeile sorgt für eine Unattendend also unbeaufsichtigte Installation.

```cmd
C:\Install> choco install notepadplusplus -y
...
```

### Tipp:
> Es funktionieren allerdings etliche Pakete auch ohne diese Berechtigungen


### Achtung:
> Insbesondere auch große Installationen wie die vom "echten" VisualStudio z.B. 2019 lassen sich auf diese Weise besonders einfach installieren.
> Allerdings braucht der installer einen zwischen Zeitlichen Reboot.
> Das äußerst sich entweder darin, das so ein Reboot (-y) automatisch vorgenommen wird, oder das nach einiger arbeit der installer mit einem Fehlercode beendet wird.
> Das ist allerdings Teil des Vorgangs sie müssen dann in beiden Fällen nach dem Rebooten die gleiche `choco install xyz` Anweisung noch einmal ausführen.
> (Ist halt doch ein Windows und kein Unix ;-))



## VirtualBox Host Installieren

Um auf einem frischen (ansonsten leeren) Windows 10 einen kompletten VirtualBox Host mit Tools zu installieren
rufen Sie bitte folgendes Script aus dem Install-Ordner auf:

```cmd
X:\Install> install-vbox-host.cmd
```

Die installation verläuft weitestgehens ohne Benutzer interaktionen, außnahmen:
- Evince PDF Viewer
- Wenn die Chrome Browser im Google Chrome Web Store ein Plugin/Theme wählen muss dieses manuell durch klick auf den Button <Hinzufügen> geschehen!





